1. We need to run Migrations to create database schema
    php artisan migrate

2. We need to run seeders so we can import master data in database
    php artisan db:seet --class=LoanEntitiesSeeder
	php artisan db:seet --class=UsersTableSeeder

3. Need to do login to access the APIs. Login API is required email as username & password, in response we get the Authorization token which we can use to access other APIs
127.0.0.1:8000/api/login

below is user 1 (customer type)
{
    "email":"customer1@assessment.com",
    "password":"customer1"
}

below is user 1 (admin type)
{
    "email":"admin@assessment.com",
    "password":"Admin"
}

Response
{
    "user": {
        "id": 1,.
        "name": "Customer 1",
        "email": "customer1@assessment.com",
        "email_verified_at": null,
        "is_customer": "1",
        "created_at": null,
        "updated_at": null
    },
    "token": "18|genSYqaSI8hxJ4IyKTlnL92pJuJd3N7E2xNWM6Yi"
}

4. User can make new loan request with this API (its a post request)
127.0.0.1:8000/api/loan_request/
        -- Only login user make a loan request
		-- Only a customer type of user can make the loan request
		-- Below parameter we required for this API, we change loan title in our master table if its valid, if not value then send an error message to end user. Loan amount already kept in master table (loan_entities) so pick from there
		-- Once loan and schedule is saved in db, in response we communiate to end user also. below are post parameter
				{
					"title":"Loan 1",
					"term":"5"
				}

5. Users (admin & customers) can see loans information with useing below get api(login required), customer can see his own loans only, admin can see all the loans
127.0.0.1:8000/api/get_loan_details/

		-- Only login user make a loan details call
		-- Customer & Admin both type of users can make the loan details
		-- Customer can see only his loans and Admin can see all the loans
		
6. User (admin) can make a loan approval / reject call (post request) with below URL:
127.0.0.1:8000/api/loan_approve/
		-- Only login user make a loan request
		-- Only an admin type of user can make the loan approval / reject call
		-- User can approve loan with pending state loans only, else in reponse we show, if it is already "APROVED" OR "REJECTED" 
		-- We also check if loan is valid or not again the database.
		-- Below parameter we required for this API
				{
					"loan_id":"1",
					"action":"APPROVED"
				}
				
7. User (customer) can make a loan repayment call (post call) with below URL
127.0.0.1:8000/api/loan_repayment/
		-- Only login user make a loan request
		-- Only a customer type of user can make the loan repayment call
		-- User can make loan repayment with pending schedule state loans only, else in reponse we show, if it is already "PAID" 
		-- Below parameter we required for this API
			{
				"loan_id":"1",
				"repayment_id": "1"
			}
			
8. POSTMAN collection file also included which will help to use the APIs, still if we have any query, please feel free to write me back.

9. Laravel 8 is used for this application & MySQL as RDBMS system.
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Loan;
use App\Models\LoanEntity;
use App\Models\LoanSchedule;


class LoanController extends Controller
{
    //
    function loan_request(Request $request){
        $ret_val=[];
        $loan_details_var=[];
        if ($request->session()->has('user')) {
            $loan_details=[];
            $user_details = $request->session()->get('user');
            if($user_details->is_customer==1){
                $Loan = new Loan();
                $loan_details = LoanEntity::where('title', $request->title)->first();
                if($loan_details){
                    
                    $Loan->customer_id = $user_details->id;
                    $Loan->approved_by = '0';
                    $Loan->loan_entity_id = $loan_details->id;
                    $Loan->term = $request->term;
                    $Loan->state = 'PENDING';
                    $Loan->created_at = date('Y-m-d H:i:s');
                    $Loan->save();
                    
                    $loan_details_var['title']=$request->title;
                    $loan_details_var['term']=$request->term;
                    $loan_details_var['loan_id']=$Loan->id;
                    $loan_details_var['state']='PENDING';
                    $loan_details_var['repayment_schedule']=[];
                    
                    $amount_val=($loan_details->amount/$request->term);
                    $LoanSchedule = new LoanSchedule();
                    for($counter=1;$counter<=$request->term;$counter++){
                        $repayment_date=date("Y-m-d",strtotime("+".($counter*7)." day", strtotime(date('Y-m-d'))));
                        $LoanSchedule->customer_id = $user_details->id;
                        $LoanSchedule->loan_id = $Loan->id;
                        $LoanSchedule->repayment_date = $repayment_date;
                        $LoanSchedule->amount = $amount_val;
                        $LoanSchedule->state = 'PENDING';
                        $LoanSchedule->created_at = date('Y-m-d H:i:s');
                        
                        
                        $loan_details_var['repayment_schedule'][$counter]['repayment_date']=$repayment_date;
                        $loan_details_var['repayment_schedule'][$counter]['amount']=$amount_val;
                        $loan_details_var['repayment_schedule'][$counter]['state']='PENDING';
                    }
                    $LoanSchedule->save();
                    $ret_val=['status'=>'Success', 'loan_details'=> $loan_details_var];
                } else {
                    $ret_val=['status'=>'Error', 'Message'=>'Incorrect loan title'];    
                }
            } else {
                $ret_val=['status'=>'Error', 'Message'=>'Only a customer can make a loan request.'];
            }
        } else {
            $ret_val=['status'=>'Error', 'Message'=>'Please do login to make loan request.'];
        }
        return response($ret_val, ($ret_val['status']=='Error'?404:200));
    }

    function loan_approve(Request $request){
        $ret_val=[];
        if ($request->session()->has('user')) {
            $loan_details=[];
            $user_details = $request->session()->get('user');
            if($user_details->is_customer!=1){
                $Loan = Loan::where('id',$request->loan_id)->first();
                if($Loan){
                    if($Loan->state=='PENDING'){
                        $Loan->state = $request->action;
                        $Loan->updated_at = date('Y-m-d H:i:s');
                        $Loan->save();
                        $ret_val=['status'=>'Success', 'Message'=>'Loan '.$request->action.' successfully.'];
                    } else {
                        $ret_val=['status'=>'Error', 'Message'=>'Cannot make loan approval request, it is in '.$Loan->state.' state'];    
                    }
                } else {
                    $ret_val=['status'=>'Error', 'Message'=>'Invalid loan id'];    
                }
            } else {
                $ret_val=['status'=>'Error', 'Message'=>'Only an admin can make a loan approval request.'];
            }
        } else {
            $ret_val=['status'=>'Error', 'Message'=>'Please do login to make loan request.'];
        }
        return response($ret_val, ($ret_val['status']=='Error'?404:200));
    }

    function get_loan_details(Request $request){
        $ret_val=[];
        if ($request->session()->has('user')) {
            $user_details = $request->session()->get('user');
            if($user_details->is_customer==1){
                $Loans = Loan::with('loan_entity')->with('loan_schedules')->where('loans.customer_id',$user_details->id)->get();
            } else {
                $Loans = Loan::with('loan_entity')->with('loan_schedules')->get();
            }

            foreach($Loans as $key => $loan){
                $ret_val[$key]['loan_id']=$loan->id;
                $ret_val[$key]['title']=$loan->loan_entity->title;
                $ret_val[$key]['state']=$loan->state;
                foreach($loan->loan_schedules as $key1 => $loan_schedule){
                    $ret_val[$key]['loan_schedule'][$key1]['repayment_id']=$loan_schedule->id;
                    $ret_val[$key]['loan_schedule'][$key1]['state']=$loan_schedule->state;
                    $ret_val[$key]['loan_schedule'][$key1]['repayment_date']=$loan_schedule->repayment_date;
                    $ret_val[$key]['loan_schedule'][$key1]['amount']=$loan_schedule->amount;
                }
            }
            $ret_val=['status'=>'Success', 'loan_details'=> $ret_val];
        } else {
            $ret_val=['status'=>'Error', 'Message'=>'Please do login to make loan request.'];
        }
        return response($ret_val, ($ret_val['status']=='Error'?404:200));
    }

    function loan_repayment(Request $request){
        $ret_val=[];
        if ($request->session()->has('user')) {
            $user_details = $request->session()->get('user');
            if($user_details->is_customer==1){
                $LoanSchedule = LoanSchedule::where('id',$request->repayment_id)->first();
                if($LoanSchedule){
                    if($LoanSchedule->state=='PENDING'){
                        $LoanSchedule->state = 'PAID';
                        $LoanSchedule->updated_at = date('Y-m-d H:i:s');
                        $LoanSchedule->save();

                        $LoanSchedules = LoanSchedule::where('loan_id',$request->loan_id)->get();
                        if($LoanSchedules){
                            $is_paid=true;
                            foreach($LoanSchedules as $LoanSchedule){
                                if($LoanSchedule->state!='PAID'){
                                    $is_paid=false;
                                    break;
                                }
                            }
                        }
                        $ret_val=['status'=>'Success', 'Message'=>'Repayment PAID successfully.'];

                        // if all repayments are done then mark the loan as paid, with below code block
                        if($is_paid==true){
                            $Loan = Loan::where('id',$request->loan_id)->first();
                            if($Loan){
                                $Loan->state = 'PAID';
                                $Loan->updated_at = date('Y-m-d H:i:s');
                                $Loan->save();
                                $ret_val=['status'=>'Success', 'Message'=>'Repayment & Loan PAID successfully.'];
                            }
                        }
                    } else {
                        $ret_val=['status'=>'Error', 'Message'=>'Current repayment id already PAID.'];    
                    }
                } else {
                    $ret_val=['status'=>'Error', 'Message'=>'Invalid repayment id'];    
                }
            } else {
                $ret_val=['status'=>'Error', 'Message'=>'Only a customer can make a loan repayment request.'];
            }
        } else {
            $ret_val=['status'=>'Error', 'Message'=>'Please do login to make loan request.'];
        }
        return response($ret_val, ($ret_val['status']=='Error'?404:200));
    }
}
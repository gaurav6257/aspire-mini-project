<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanEntityController extends Controller
{
    //
    function list(Request $request){
        $user_details = $request->session()->get('user');
        if($user_details['is_customer']==0){
            return DB::select("select * from loan_entities");             // if current user is admin
        }
        return [];      // if current user is customer
    }
}

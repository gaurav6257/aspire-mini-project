<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanScheduleController extends Controller
{
    //
    function show(){
        return view('loans');
    }

    function index(){
        return DB::select("select * from loans");
    }

    function list(){
        return DB::select("select * from loan_schedules");
    }
}

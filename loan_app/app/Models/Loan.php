<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Loan extends Model
{
    use HasFactory;

    public function loan_entity(){
        return $this->belongsTo(LoanEntity::class);
    }

    public function loan_schedules(){
        return $this->hasMany(LoanSchedule::class);
    }
}

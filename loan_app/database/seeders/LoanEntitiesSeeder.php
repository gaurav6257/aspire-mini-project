<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LoanEntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('loan_entities')->insert(['title'=>'Loan 1','amount'=>10000]);
        DB::table('loan_entities')->insert(['title'=>'Loan 2','amount'=>50000]);
        DB::table('loan_entities')->insert(['title'=>'Loan 4','amount'=>15000]);
        DB::table('loan_entities')->insert(['title'=>'Loan 5','amount'=>20000]);
    }
}

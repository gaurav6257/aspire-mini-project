<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([ 'name' => 'Customer 1', 'email' => 'customer1@assessment.com', 'password' => Hash::make('customer1'), 'is_customer' => 1 ]);
        DB::table('users')->insert([ 'name' => 'Customer 2', 'email' => 'customer2@assessment.com', 'password' => Hash::make('customer1'), 'is_customer' => 1 ]);
        DB::table('users')->insert([ 'name' => 'Admin', 'email' => 'admin@assessment.com', 'password' => Hash::make('Admin'), 'is_customer' => 0 ]);
    }
}

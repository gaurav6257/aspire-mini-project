<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoanEntityController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\LoanScheduleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("login",[UserController::class,'index']);
Route::group(['middleware' => 'auth:sanctum'], function(){
    //All secure URL's    
    Route::post('loan_request', [LoanController::class, 'loan_request']);
    Route::post('loan_approve', [LoanController::class, 'loan_approve']);
    Route::post('loan_repayment', [LoanController::class, 'loan_repayment']);
    Route::get('get_loan_details', [LoanController::class, 'get_loan_details']);
});